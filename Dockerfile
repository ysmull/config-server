FROM oracle/serverjre:8
COPY target/config-server-1.0-SNAPSHOT.jar /
CMD [ "java", "-jar",  "config-server-1.0-SNAPSHOT.jar"]