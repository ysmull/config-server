#!/usr/bin/env bash
pwd
echo "-trying to stop container..."
sudo docker stop config-server
echo "-trying to rm container..."
sudo docker rm config-server
echo "-trying to rmi image..."
sudo docker rmi config-server
echo "-building image..."
sudo docker build -t config-server .
echo "-running image..."
echo "-expose port 7777"
sudo docker run -d --name="config-server" -p 7777:8888 config-server
echo "-assign ip address 172.17.0.111 172.17.0.111/24@172.17.0.1"
sudo pipework docker0 config-server 172.17.0.111/24@172.17.0.1


